package main

import (
	"errors"
	"fmt"
	"os"
)

const resultFile = "result.txt"

func main() {

	grossRevenue, err := getUserInput("Gross revenue: ")
	if err != nil {
		panic(err)
	}
	taxRate, err := getUserInput("Tax Rate: ")
	if err != nil {
		panic(err)
	}
	expenses, err := getUserInput("Expenses: ")
	if err != nil {
		panic(err)
	}

	ebt, profit, ratio := calculateFinancials(grossRevenue, taxRate, expenses)

	fmt.Printf("EBT: %.1f\n", ebt)
	fmt.Printf("Profit: %.1f\n", profit)
	fmt.Printf("Ratio: %.3f\n", ratio)
	writeResultToFile(ebt, profit, ratio)
}

func calculateFinancials(grossRevenue, taxRate, expenses float64) (ebt, profit, ratio float64) {
	ebt = grossRevenue - expenses
	profit = ebt * (1 - taxRate/100)
	ratio = ebt / profit
	return ebt, profit, ratio
}

func getUserInput(infoText string) (float64, error) {
	var userInput float64
	fmt.Print(infoText)
	fmt.Scan(&userInput)
	if userInput <= 0 {
		return 0, errors.New("invalid input. must be greater than zero")
	}
	return userInput, nil
}

func writeResultToFile(ebt, profit, ratio float64) {

	resultText := fmt.Sprintf("EBT: %.1f\t Profit: %.1f\t Ratio: %.3f\n", ebt, profit, ratio)
	os.WriteFile(resultFile, []byte(resultText), 0644)
}
